package pa;

/**
 * This class represents a simple picture. You can draw the picture using
 * the draw method. But wait, there's more: being an electronic picture, it
 * can be changed. You can set it to black-and-white display and back to
 * colors (only after it's been drawn, of course).
 *
 * This class was written as an early example for teaching Java with BlueJ.
 * 
 * @author  Michael K�lling and David J. Barnes
 * @version 2016.02.29
 */
public class Picture
{
    private Square wall;
    private Square wallt;
    private Square window;
    private Square windowt;
    private Triangle roof;
    private Triangle rooft;
    private Circle sun;
    private Person person;
    private boolean drawn;

    /**
     * Constructor for objects of class Picture
     */
    public Picture()
    {
        wall = new Square();
        wallt = new Square();
        window = new Square();
        windowt = new Square();
        roof = new Triangle();
        rooft = new Triangle();
        sun = new Circle();
        person = new Person();
        drawn = false;
    }

    /**
     * Draw this picture.
     */
    public void draw()
    {
        if(!drawn) {
            wall.moveHorizontal(-200);
            wall.moveVertical(10);
            wall.changeSize(120);
            wall.makeVisible();
            
            wallt.moveHorizontal(20);
            wallt.moveVertical(10);
            wallt.changeSize(120);
            wallt.makeVisible();
            
            window.changeColor("black");
            window.moveHorizontal(-180);
            window.moveVertical(30);
            window.changeSize(40);
            window.makeVisible();
            
            windowt.changeColor("black");
            windowt.moveHorizontal(40);
            windowt.moveVertical(30);
            windowt.changeSize(40);
            windowt.makeVisible();
    
            roof.changeSize(60, 180);
            roof.moveHorizontal(-40);
            roof.moveVertical(-70);
            roof.makeVisible();
            
            rooft.changeSize(60, 180);
            rooft.moveHorizontal(180);
            rooft.moveVertical(-70);
            rooft.makeVisible();
            
            person.changeColor("black");
            person.makeVisible();
            
    
            sun.changeColor("yellow");
            sun.moveHorizontal(60);
            sun.moveVertical(-90);
            sun.changeSize(80);
            sun.makeVisible();
            drawn = true;
        }
    }

    /**
     * Change this picture to black/white display
     */
    public void setBlackAndWhite()
    {
        wall.changeColor("black");
        window.changeColor("white");
        roof.changeColor("black");
        sun.changeColor("black");
    }

    /**
     * Change this picture to use color display
     */
    public void setColor()
    {
        wall.changeColor("red");
        window.changeColor("black");
        roof.changeColor("green");
        sun.changeColor("yellow");
    }
}
