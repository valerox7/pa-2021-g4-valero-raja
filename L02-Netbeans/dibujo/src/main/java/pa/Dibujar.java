/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa;


public class Dibujar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //System.out.println("Ejercicio pendiente de realizar");
        
        Circle c1; //Declarar la variable c1, de tipo Circle
        c1 = new Circle();
        c1.moveVertical(-50);
        c1.changeColor("yellow");
        c1.makeVisible();
        
        Square s1;
        s1 = new Square();
        s1.moveHorizontal(-230);
        s1.moveVertical(40);
        s1.changeColor("blue");
        s1.makeVisible();
        
        Square s2;
        s2 = new Square();
        s2.moveHorizontal(10);
        s2.moveVertical(40);
        s2.changeColor("green");
        s2.makeVisible();
        
        Triangle t1;
        t1 = new Triangle();
        t1.moveHorizontal(-100);
        t1.moveVertical(-40);
        t1.changeColor("red");
        t1.makeVisible();
        
        Triangle t2;
        t2 = new Triangle();
        t2.moveHorizontal(140);
        t2.moveVertical(-40);
        t2.changeColor("red");
        t2.makeVisible();
        
        Person p1;
        p1 = new Person();
        p1.moveHorizontal(-50);
        p1.moveVertical(-12);
        p1.makeVisible();
        
    }
    
}
